## Projet de gestion de budget entreprise

On a travaillé en groupe de 4 sur ce projet qui consistait à faire la conception d'une application de gestion de budget d'entreprise.

 En le représentant avec : 

 - Un Use Case
 - Un diagramme de classe avec ses méthodes


Dans le diagramme de classe, nous avons choisi de créer 6 entités :

- Salarié, qui permet d'identifier différents acteurs de l'entreprise
- Commande, qui va permettre de gérer la quantité, le statut de la commande.
- Historique, permet de récupérer toutes les commandes passées au sein d'un service.
- Item, représente un élément d'une commande.
- Category, permet catégoriser les éléments d'une commande.
- CategoryService, permet de lister les différents services de l'entreprise afin de trier l'historique en fonction du service.
- Budget


Ne souhaitant pas que les propriétés de nos entités soient modifiable ou visible par d'autres utlisateurs, nous les avons mis en private.

Nous avons choisi ces noms de méthodes parcequ'ils étaient explicites. 

Par exemple, dans le repository de la class Commande, nous retrouvons la méthode "isValidate()" qui nous permettra de valider la commande.

[Jehane](https://gitlab.com/ghanemi.jehane.simplon)
[Rafaël](https://gitlab.com/rafasoler117)
[Andy](https://gitlab.com/AndyMez)
[Thanhmy](https://gitlab.com/Thanhmymy)
